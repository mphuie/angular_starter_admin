app.controller('HomeCtrl', function($scope, $http, dataFactory, toaster) {
    console.log('hi from home!');

    // types: success, error, wait, warning, note
    toaster.pop('success', 'title', 'example toast')

    dataFactory
        .then(function(response) {
            $scope.ip = response.data.ip;
        },
        function(response) {
            console.log('Error getting response!');
        });

    $http
        .get('http://headers.jsontest.com')
        .then(function(response) {
            $scope.headers = response;
        },
        function(response) {
        })
});