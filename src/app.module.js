'use strict';

var app = angular.module('myApp', ['ui.router', 'ui.bootstrap', 'toaster', 'ngAnimate']);

app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: '/partials/home.html',
            controller: 'HomeCtrl',
        })
        .state('test', {
            url: '/test',
            templateUrl: '/partials/test.html'
        })

});